package ch02;

public class Casting {
	public static void main(String[] args) {
		System.out.println((int)('A')); // 65
		 System.out.println((short)('a')); // 97
		 System.out.println((char)(95)); // _
		 System.out.println((int)(1.414)); // 1
		 System.out.println((float)(10)); // 10.0
		 
		 long num1 = 100L;
		 int num2 = (int) num1; // 강제 형변환
		 System.out.println(num2);

		 int num3 = 100;
		 float num4 = 2.2f;
		 // 자동 형변환
		 System.out.println((num3 * num4)); 
	}
}
