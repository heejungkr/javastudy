package ch03;

public class Operation4 {
	 public static void main(String[] args) {
	 int num1 = 0;
	 int num2 = 0;
	 boolean result;

	 /* Short-Circuit Evaluation */
	 result = num1++ < 0 && num2++ > 0;
	 System.out.println("result : " + result);
	 System.out.println("num1 : " +
	 num1 + ", num2 : " + num2);

	 result = num1++ > 0 || num2++ > 0;
	 System.out.println("result : " + result);
	 System.out.println("num1 : " +
	 num1 + ", num2 : " + num2);
	 
	 int num3 = 0;
	 int num4 = 0;
	 result = num3++ < 0 & num4++ > 0;
	 System.out.println("result : " + result);
	 System.out.println("num3 : " +
	 num3 + ", num4 : " + num4);

	 result = num3++ > 0 | num4++ > 0;
	 System.out.println("result : " + result);
	 System.out.println("num3 : " +
	 num3 + ", num4 : " + num4);
	 }
	}
